[![Maintenance](https://img.shields.io/badge/maintenance-unreleased-critical?style=for-the-badge)]()
[![License](https://img.shields.io/badge/license-MIT-informational?style=for-the-badge)](./LICENSE.md)
[![Coverage](https://img.shields.io/gitlab/coverage/detly/calloop-zmq/main?style=for-the-badge)]()
[![Rust](https://img.shields.io/badge/rust-^1.56-informational?style=for-the-badge)]()

# calloop-zmq

<!-- cargo-rdme start -->

ZeroMQ event source for the [Calloop event loop][calloop].

[calloop]: https://crates.io/crates/calloop

Calloop is built around the concept of *event sources* eg. timers, channels,
file descriptors themselves, which generate events and call a callback for
them. This crate provides two kinds of event sources for the ZeroMQ message
queue library:

<!-- cargo-rdme end -->

---

Minimum supported Rust version: 1.56. Licensed under the MIT license (see `LICENSE` file in this directory).

This document is kept up-to-date with [cargo-rdme](https://github.com/orium/cargo-rdme).
