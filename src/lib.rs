//! ZeroMQ event source for the [Calloop event loop][calloop].
//!
//! [calloop]: https://crates.io/crates/calloop
//!
//! Calloop is built around the concept of *event sources* eg. timers, channels,
//! file descriptors themselves, which generate events and call a callback for
//! them. This crate provides two kinds of event sources for the ZeroMQ message
//! queue library:

#![forbid(unsafe_code)]

pub mod wrapped;
pub mod zmqsource;

pub use wrapped::{WrappedSocket, ZmqDir};
pub use zmqsource::ZeroMQSource;
