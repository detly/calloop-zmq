//! A Calloop event source implementation for ZeroMQ sockets.

use std::collections;

use anyhow::Context;
use calloop::{generic::Generic, transient::TransientSource};
use log::*;

use super::wrapped::{WrappedSocket, ZeroMqFd};

// You'll see a lot of this trait bound below:
//
//     T where T: IntoIterator, T::Item: Into<zmq::Message>
//
// See the docs for `ZeroMQSource` for an explanation. One day we can alias a
// trait bound for it. See: https://github.com/rust-lang/rfcs/issues/804

/// A Calloop event source that contains a ZeroMQ socket (of any kind) and a
/// Calloop MPSC channel for sending over it.
///
/// The basic interface is:
/// - create a WrappedSocket for your ZeroMQ socket
/// - use `ZeroMQSource::from_socket()` to turn it into a Calloop event source
///   (plus the sending end of the channel)
/// - queue messages to be sent by calling `ZeroMQSource::send()`
/// - add the event source to the Calloop event loop with a callback to handle
///   reading
///
/// The message type is parameterised by `T`:
///
///     T where T: IntoIterator, T::Item: Into<zmq::Message>
///
/// This means that `T` is anything that can be converted to an iterator, and
/// the items in the iterator are anything that can be converted to a
/// `zmq::Message`. So eg. a `Vec<String>` would work.
///
/// The callback is called whenever the underlying socket becomes readable. It
/// is called with a vec of ZeroMQ messages (`Vec<zmq::Message>`) and the event
/// loop data set by the user.
///
/// Note about why the read data is a vec of multipart message parts: we don't
/// know what kind of socket this is, or what will be sent, so the most general
/// thing we can do is receive the entirety of a multipart message and call the
/// user callback with the whole set. Usually the number of parts in a multipart
/// message will be one, but the code will work just the same when it's not.
///
/// This event source also allows you to use different event sources to publish
/// messages over the same writeable ZeroMQ socket (usually PUB or PUSH).
/// Messages should be sent over the Calloop MPSC channel sending end. This end
/// can be cloned and used by multiple senders. Common use cases are:
///
/// - One sender: just use `send()`. Useful for any scoket kind except `PULL` or
///   `SUB`.
/// - Multiple senders: clone the sending end of the channel with
///   `clone_sender()`. Useful for `PUSH`, `PUB`, `DEALER`, but probably not at
///   all useful for `REQ`/`REP`.
/// - No senders: take the sending end of the channel with `take_sender()` and
///   drop it. Useful for `SUB` and `PULL`.

pub struct ZeroMQSource<T> {
    /// Event source for ZeroMQ socket. This shares a reference to the
    /// underlying ZeroMQ socket in the `socket` field.
    socket_source: Generic<ZeroMqFd>,

    /// The underlying ZeroMQ socket that we're proxying things to.
    socket: WrappedSocket,

    /// Event source for channel.
    mpsc_receiver: TransientSource<calloop::channel::Channel<T>>,

    /// Sending end of channel.
    mpsc_sender: Option<calloop::channel::Sender<T>>,

    /// Because the ZeroMQ socket is edge triggered, we need a way to "wake" the
    /// event source upon (re-)registration. We do this with a separate
    /// `calloop::ping::Ping` source.
    wake_ping_receiver: calloop::ping::PingSource,

    /// Sending end of the ping source.
    wake_ping_sender: calloop::ping::Ping,

    /// FIFO queue for the messages to be published.
    outbox: collections::VecDeque<T>,
}

impl<T> ZeroMQSource<T>
where
    T: IntoIterator,
    T::Item: Into<zmq::Message>,
{
    // Converts a `WrappedSocket` into a `ZeroMQSource`.
    pub fn from_socket(socket: WrappedSocket) -> anyhow::Result<Self> {
        let (mpsc_sender, mpsc_receiver) = calloop::channel::channel();
        let (wake_ping_sender, wake_ping_receiver) = calloop::ping::make_ping()?;

        // ZeroMQ's "public" file descriptor is always edge-triggered and
        // read-only. When new events are available on the socket, whether read
        // or write (or error) the file descriptor becomes readable.
        let socket_source = Generic::new(
            socket.get_fd()?,
            calloop::Interest::READ,
            calloop::Mode::Edge,
        );

        Ok(Self {
            socket,
            socket_source,
            mpsc_receiver: mpsc_receiver.into(),
            mpsc_sender: Some(mpsc_sender),
            wake_ping_receiver,
            wake_ping_sender,
            outbox: collections::VecDeque::new(),
        })
    }

    // Send a message via the ZeroMQ socket. If the sending end has been
    // extracted then this will return an error (as well as for any other error
    // condition on a still-existing channel sending end).
    pub fn send(&self, msg: T) -> Result<(), std::sync::mpsc::SendError<T>> {
        if let Some(sender) = &self.mpsc_sender {
            sender.send(msg)
        } else {
            Err(std::sync::mpsc::SendError(msg))
        }
    }

    pub fn clone_sender(&self) -> Option<calloop::channel::Sender<T>> {
        self.mpsc_sender.clone()
    }

    // Remove the channel sending end from this ZeroMQ source and return it. If
    // you do not want to use it, you can drop it and the receiving end will be
    // disposed of too. This is useful for ZeroMQ sockets that are only for
    // incoming messages.
    pub fn take_sender(&mut self) -> Option<calloop::channel::Sender<T>> {
        self.mpsc_sender.take()
    }
}

/// This event source runs for three events:
///
/// 1. The event source was registered. It is forced to run so that any pending
///    events on the socket are processed.
///
/// 2. A message was sent over the MPSC channel. In this case we put it in the
///    internal queue.
///
/// 3. The ZeroMQ socket is readable. For this, we read off a complete multipart
///    message and call the user callback with it.
///
/// The callback provided to `process_events()` may be called multiple times
/// within a single call to `process_events()`.
impl<T> calloop::EventSource for ZeroMQSource<T>
where
    T: IntoIterator,
    T::Item: Into<zmq::Message>,
{
    type Event = Vec<zmq::Message>;
    type Metadata = ();
    type Ret = anyhow::Result<()>;
    type Error = anyhow::Error;

    fn process_events<F>(
        &mut self,
        readiness: calloop::Readiness,
        token: calloop::Token,
        mut callback: F,
    ) -> Result<calloop::PostAction, Self::Error>
    where
        F: FnMut(Self::Event, &mut Self::Metadata) -> Self::Ret,
    {
        // We were woken up on startup/registration.
        self.wake_ping_receiver
            .process_events(readiness, token, |_, _| {})?;

        // ...or we were woken up to send a new message.
        let channel_post_action =
            self.mpsc_receiver
                .process_events(readiness, token, |evt, _| {
                    if let calloop::channel::Event::Msg(msg) = evt {
                        self.outbox.push_back(msg);
                    };
                })?;

        // The ZeroMQ file descriptor is edge triggered. This means that if (a)
        // messages are added to the queue before registration, or (b) the
        // socket became writeable before messages were enqueued, we will need
        // to run the loop below. Hence, it always runs if this event source
        // fires.

        loop {
            // According to the docs, the edge-triggered FD will not change
            // state if a socket goes directly from being readable to being
            // writeable (or vice-versa) without there being an in-between point
            // where there are no events. This can happen as a result of sending
            // or receiving on the socket while processing such an event. The
            // "used_socket" flag below tracks whether we perform an operation
            // on the socket that warrants reading the events again.
            let events = self.socket.events()?;
            let mut used_socket = false;

            if events.contains(zmq::POLLOUT) {
                if let Some(parts) = self.outbox.pop_front() {
                    self.socket
                        .as_ref()
                        .send_multipart(parts, 0)
                        .context("Sending on ZeroMQ socket")?;
                    used_socket = true;
                }
            }

            if events.contains(zmq::POLLIN) {
                // Batch up multipart messages. ZeroMQ guarantees atomic message
                // sending, which includes all parts of a multipart message.
                let messages = self.socket.recv_full_message()?;
                used_socket = true;

                // Capture and report errors from the callback, but don't propagate
                // them up.
                callback(messages, &mut ()).unwrap_or_else(|err| {
                    error!("Error processing ZeroMQ message: {:#?}", err);
                })
            }

            if !used_socket {
                break;
            }
        }

        // If the socket's internal queue is full, start logging that this
        // internal queue is growing. It should only be a temporary situation,
        // so we need to know about it if it's not.
        if !self.outbox.is_empty() {
            log::trace!("{} queue size: {}", self.socket, self.outbox.len());
        }

        // The channel is the only thing that can require reregistration.
        Ok(channel_post_action)
    }

    fn register(
        &mut self,
        poll: &mut calloop::Poll,
        token_factory: &mut calloop::TokenFactory,
    ) -> calloop::Result<()> {
        self.socket_source.register(poll, token_factory)?;
        self.mpsc_receiver.register(poll, token_factory)?;
        self.wake_ping_receiver.register(poll, token_factory)?;

        self.wake_ping_sender.ping();

        Ok(())
    }

    fn reregister(
        &mut self,
        poll: &mut calloop::Poll,
        token_factory: &mut calloop::TokenFactory,
    ) -> calloop::Result<()> {
        self.socket_source.reregister(poll, token_factory)?;
        self.mpsc_receiver.reregister(poll, token_factory)?;
        self.wake_ping_receiver.reregister(poll, token_factory)?;

        self.wake_ping_sender.ping();

        Ok(())
    }

    fn unregister(&mut self, poll: &mut calloop::Poll) -> calloop::Result<()> {
        self.socket_source.unregister(poll)?;
        self.mpsc_receiver.unregister(poll)?;
        self.wake_ping_receiver.unregister(poll)?;
        Ok(())
    }
}
