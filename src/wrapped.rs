//! A ZeroMQ socket wrapped in some common abstractions for our Calloop event
//! sources.

use anyhow::Context;
use std::{
    convert, fmt,
    num::NonZeroI32,
    os::unix::io::{AsRawFd, RawFd},
    sync::Arc,
};

/// Denotes whether a ZeroMQ socket is to be a "bind" or "connect" type of
/// socket.
#[derive(Copy, Clone, Debug)]
pub enum ZmqDir {
    Bind,
    Connect,
}

/// The fd associated with a ZeroMQ socket. The docs do not specifically say
/// what kind of lifetime a socket's fd has, but they do say "fd associated
/// with the socket", and in the absence of any other information I'd assume
/// it's valid the entire time the socket is.
pub struct ZeroMqFd {
    /// This is to keep the underlying ZeroMQ socket alive while the fd is in
    /// use, and to retain our "reconfigure on drop" semantics.
    _zeromq: Arc<ZeroMqCloseOnDrop>,
    fd: RawFd,
}

pub struct ZeroMqCloseOnDrop {
    /// The underlying ZeroMQ socket.
    socket: zmq::Socket,

    /// The endpoint provided for construction. Required for disconnecting and
    /// debug representation.
    endpoint: String,

    /// Debug representation of the socket.
    debug_repr: String,
}

/// Wrapper type for zmq::Socket to handle common behaviour we want in our
/// Calloop and Rust integration.
pub struct WrappedSocket {
    /// It is wrapped in an Arc so that ownership can be shared with any fd
    /// wrappers created.
    zeromq: Arc<ZeroMqCloseOnDrop>,
}

impl WrappedSocket {
    pub fn new(context: &zmq::Context, endpoint: &str, kind: zmq::SocketType, dir: ZmqDir) -> Self {
        let zeromq = context
            .socket(kind)
            .unwrap_or_else(|_| panic!("Could not open socket {}", endpoint));

        zeromq.set_linger(-1).ok();

        match dir {
            ZmqDir::Bind => zeromq
                .bind(endpoint)
                .unwrap_or_else(|_| panic!("Could not bind socket {}", endpoint)),
            ZmqDir::Connect => zeromq
                .connect(endpoint)
                .unwrap_or_else(|_| panic!("Could not connect socket {}", endpoint)),
        }

        let inner = ZeroMqCloseOnDrop {
            socket: zeromq,
            endpoint: endpoint.to_owned(),
            debug_repr: format!(
                "wrapped ZeroMQ {:?} ({:?}) socket on {}",
                kind, dir, endpoint
            ),
        };

        Self {
            zeromq: Arc::new(inner),
        }
    }

    /// Clones the Arc-wrapped socket that we're wrapping.
    pub(crate) fn get_fd(&self) -> anyhow::Result<ZeroMqFd> {
        let fd = self
            .zeromq
            .socket
            .get_fd()
            .with_context(|| format!("Could not obtain fd for {}", self.zeromq))?;

        Ok(ZeroMqFd {
            _zeromq: self.zeromq.clone(),
            fd,
        })
    }

    /// Sets the receiving "high water mark" which is the ZeroMQ socket's
    /// internal queue, in units of messages. See the ZeroMQ documentation for
    /// details of this.
    pub fn set_receive_hwm(&self, hwm: Option<NonZeroI32>) -> anyhow::Result<()> {
        let arg = hwm.map(core::num::NonZeroI32::get).unwrap_or_default();

        self.zeromq
            .socket
            .set_rcvhwm(arg)
            .context("Setting ZeroMQ receiving HWM")
    }

    /// Sets the sending "high water mark" which is the ZeroMQ socket's internal
    /// queue, in units of messages. See the ZeroMQ documentation for details of
    /// this.
    pub fn set_send_hwm(&self, hwm: Option<NonZeroI32>) -> anyhow::Result<()> {
        let arg = hwm.map(core::num::NonZeroI32::get).unwrap_or_default();

        self.zeromq
            .socket
            .set_sndhwm(arg)
            .context("Setting ZeroMQ sending HWM")
    }

    pub fn can_read(&self) -> anyhow::Result<bool> {
        Ok(self
            .zeromq
            .socket
            .get_events()
            .context("Checking ZeroMQ read readiness")?
            .contains(zmq::POLLIN))
    }

    pub fn can_write(&self) -> anyhow::Result<bool> {
        Ok(self
            .zeromq
            .socket
            .get_events()
            .context("Checking ZeroMQ write readiness")?
            .contains(zmq::POLLOUT))
    }

    pub fn events(&self) -> anyhow::Result<zmq::PollEvents> {
        self.zeromq
            .socket
            .get_events()
            .context("Checking ZeroMQ read/write readiness")
    }

    pub fn recv_full_message(&self) -> anyhow::Result<Vec<zmq::Message>> {
        let mut messages = vec![];

        loop {
            let message = self
                .zeromq
                .socket
                .recv_msg(0)
                .context("Receiving ZeroMQ message")?;

            messages.push(message);

            if !self.zeromq.socket.get_rcvmore()? {
                break;
            }
        }

        Ok(messages)
    }
}

impl AsRawFd for ZeroMqFd {
    fn as_raw_fd(&self) -> RawFd {
        self.fd
    }
}

impl Drop for ZeroMqCloseOnDrop {
    fn drop(&mut self) {
        // This is necessary to prevent hanging in the disconnect call
        // (especially for PUSH sockets). If it fails, we panic rather than
        // allow a program to continue and potentially block indefinitely.
        //
        // - https://stackoverflow.com/a/38338578/188535
        // - http://api.zeromq.org/4-0:zmq-disconnect
        // - http://api.zeromq.org/4-0:zmq-ctx-term
        self.socket
            .set_linger(0)
            .unwrap_or_else(|e| panic!("Could not set linger to 0 for {}: {}", self, e));

        // The other calls are not so critical if they fail that we should panic
        // in a drop handler.
        self.socket.set_rcvtimeo(0).unwrap_or_else(|e| {
            log::error!("Could not set receive timeout to zero on {}: {}", self, e)
        });

        self.socket.set_sndtimeo(0).unwrap_or_else(|e| {
            log::error!("Could not set send timeout to zero on {}: {}", self, e)
        });

        if let Err(e) = self.socket.disconnect(&self.endpoint) {
            // This is actually fine, it just means the endpoint is already
            // disconnected. From the manual for zmq_disconnect:
            //
            // > ENOENT
            // >   The provided endpoint is not connected.
            if e == zmq::Error::ENOENT {
                log::info!("Endpoint already disconnected for {}", self);
            } else {
                log::error!("Could not disconnect {}: {}", self, e);
            }
        }
    }
}

impl fmt::Display for ZeroMqCloseOnDrop {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.debug_repr)
    }
}

impl fmt::Display for WrappedSocket {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.zeromq.fmt(f)
    }
}

// TODO: these are here so we can gradually move more functionality into this
// wrapper. Eventually they should not be necessary at all.

impl convert::AsRef<zmq::Socket> for WrappedSocket {
    fn as_ref(&self) -> &zmq::Socket {
        &self.zeromq.socket
    }
}
